from vivpy.viv_links.instrument_link import InstrumentLink, InstrumentConnectionConfig
from vivpy.proto.headers.market.simple_types_pb2 import INSTR_KIND_CALL, INSTR_KIND_PUT, INSTR_KIND_FUTURE
import time
from datetime import date
from instrument_functions import Option, is_itm, get_option_type, strike_distance, get_monthly_expiries
import sys
import traceback
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")
config = config["HOSTS"]

instr_config = InstrumentConnectionConfig(config["PEANUT_HOST"], int(config["PEANUT_PORT"]), 
    config["THEO_SERVER_HOST"], int(config["THEO_SERVER_PORT"]))

# Test params These should be input with command line arguments
underlying_prices = {
    "HHI": 9450,
    "MCH": 9450,
    "HSI": 22800,
    "MHI": 22800
}

def clear_offsets(instr_def, instr_link, expiries):
    feedcode = instr_def.instr_id.feedcode
    offset_fields = ["bid_offset_override", "ask_offset_override"]
    params = dict.fromkeys(offset_fields, 0)

    instr_link.set_params("XHKF", feedcode, params)

def set_strike_params(instr_def, instr_link, expiries):
    feedcode = instr_def.instr_id.feedcode
    expr = instr_def.expiry_date
    expr_dt = date(expr.year, expr.month, expr.day)

    option_type = get_option_type(instr_def)

    # Flag fields to all set to False or True
    flag_fields = ["autotrade_buy", "autotrade_sell"]

    # This is to determine if the strike is in the quoting obligations pool
    itm = is_itm(instr_def, underlying_prices)
    strike_dist = strike_distance(instr_def, underlying_prices)

    #cq_strike means that it is a strike that counts towards our CQ obligations
    cq_strike = (itm and strike_dist <= 2) or (not itm and strike_dist <= 15)

    # Default behaviour is to turn off quote flags and set volume to 0
    quote_volume = 0
    flags = dict.fromkeys(flag_fields, False)

    # We only need to quote the first 2/4 months for the minis/monthly
    if option_type == Option.MONTHLY:
        if expr_dt in expiries and expiries.index(expr_dt) <= 3:
            quote_volume = 7
            
            if cq_strike:
                flags = dict.fromkeys(flag_fields,True)
        else:
            quote_volume = 5
    elif option_type == Option.MINI:
        if expr_dt in expiries and expiries.index(expr_dt) <= 1:
            if cq_strike:
                flags = dict.fromkeys(flag_fields, True)

        quote_volume = 5
    
    instr_link.set_params("XHKF", feedcode, {"std_order_volume": quote_volume})
    instr_link.set_flags("XHKF", feedcode, flags)



def main():
    # Need to check what HK profile actually means
    instr_link = InstrumentLink("HK", instr_config)

    # Waits for Peanut to send over all instrument definitions. Should be a cleaner way to do this.
    # Ideally we get some sort of signal from InstrumentLink to indicate that it's been downloaded
    while len(instr_link.list_instruments()) == 0:
        print("sleeping")
        time.sleep(1)

    expiries = get_monthly_expiries(instr_link)

    for instr in instr_link.list_instruments():
        instr_des = instr_link.get_instrument(*instr)
        instr_def = instr_des.definition

        if instr_def.kind == INSTR_KIND_PUT or instr_def.kind == INSTR_KIND_CALL:
            set_strike_params(instr_def, instr_link, expiries)
            clear_offsets(instr_def, instr_link, expiries)

    print("Submitting updates")
    instr_link.submit_updates()
    print("Finished submitting updates")
                            

if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
