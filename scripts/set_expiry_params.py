from vivpy.viv_links.expiry_link import ExpiryLink, ExpiryConnectionConfig
import configparser
import traceback
from time import sleep

config = configparser.ConfigParser()
config.read("../config.ini")
config = config["HOSTS"]

conn_config = ExpiryConnectionConfig(config["PEANUT_HOST"], int(config["PEANUT_PORT"]))

expiry_link = ExpiryLink("HK", conn_config)

sleep(1)


params = {
    "cv_atm_vega_size": 6,
    "cv_min_volume": 6,
    "cv_max_volume": 6,
    "cv_itm_volume": 6,

    "co_min_edge": 0.8,
    "co_vega_factor": 0.02,
    "co_delta_factor": 8,
    "co_position_factor": 1000,
}

def main():
	expiries = sorted(set(x[1] for x in expiry_link.list_expiries()))

	print(expiries)

if __name__ == "__main__":
	try:
		main()
	except:
		traceback.print_exc()


