import enum
from datetime import date
from vivpy.proto.headers.market.simple_types_pb2 import INSTR_KIND_CALL, INSTR_KIND_PUT, INSTR_KIND_FUTURE
import math

class Option(enum.Enum):
    MONTHLY = 1
    MINI = 2
    WEEKLY = 3
    UNKNOWN = 4

def get_option_type(instr_def):
    underlying = instr_def.underlying
    feedcode = instr_def.instr_id.feedcode

    if underlying == "MHI" or underlying == "MCH":
        return Option.MINI
    elif feedcode[-3] == "W":
        return Option.WEEKLY
    elif underlying == "HSI" or underlying == "HHI":
        return Option.MONTHLY
    else:
        return Option.UNKNOWN

def strike_distance(instr_def, underlying_prices):
    underlying = underlying_prices[instr_def.underlying]
    strike = instr_def.strike_price

    if strike >= 20000 and underlying >= 20000:
        dist = abs(strike - underlying)/200
    elif strike <= 20000 and underlying <= 20000:
        dist = abs(strike - underlying)/100
    elif strike >= 20000 and underlying <= 20000:
        dist = abs(strike - 20000)/200 + abs(20000 - underlying)/100
    elif strike <= 20000 and underlying >= 20000:
        dist = abs(underlying - 20000)/200 + abs(20000 - strike)/100

    return math.ceil(dist)

def is_itm(instr_def, underlying_prices):
    underlying_price = underlying_prices[instr_def.underlying]

    if instr_def.kind == INSTR_KIND_CALL:
        if instr_def.strike_price < underlying_price:
            return True
        else:
            return False
    elif instr_def.kind == INSTR_KIND_PUT:
        if instr_def.strike_price > underlying_price:
            return True
        else:
            return False

    return False

def get_monthly_expiries(instr_link):
    expiries = set()

    for instr in instr_link.list_instruments():
        instr_des = instr_link.get_instrument(*instr)
        instr_def = instr_des.definition
        feedcode = instr_def.instr_id.feedcode

        expiry = instr_def.expiry_date
        expiry_dt = date(expiry.year, expiry.month, expiry.day)

        # This should exclude expiring months
        if expiry_dt > date.today() and get_option_type(instr_def) == Option.MONTHLY:
            expiries.add(expiry_dt)

    return sorted(expiries)