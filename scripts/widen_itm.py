from vivpy.viv_links.instrument_link import InstrumentLink, InstrumentConnectionConfig
from vivpy.proto.headers.market.simple_types_pb2 import INSTR_KIND_CALL, INSTR_KIND_PUT, INSTR_KIND_FUTURE
import time
from datetime import date
from instrument_functions import Option, is_itm, get_option_type, strike_distance, get_monthly_expiries
import sys
import traceback
import configparser

config = configparser.ConfigParser()
config.read("../config.ini")
config = config["HOSTS"]

instr_config = InstrumentConnectionConfig(config["PEANUT_HOST"], int(config["PEANUT_PORT"]), 
    config["THEO_SERVER_HOST"], int(config["THEO_SERVER_PORT"]))

# Defined in scope level.
# multiplier = quote_widener * widener
multiplier = 6

# Dict specifies the ITM offsets for each monthly expiry.
# 0 refers to the first monthly expiry and so on. 
# This assumes the widener is 6 so that we still meet quoting obligations.
offset_dict = {
	0: 5.5,
	1: 5.5,
	2: 5.5,
	3: 5.5,
	4: 8.5,
	5: 8.5,
	6: 11
}

def main():
    instr_link = InstrumentLink("HK", instr_config)

    while len(instr_link.list_instruments()) == 0:
        print("sleeping")
        time.sleep(1)

    expiries = get_monthly_expiries(instr_link)
    print(expiries)

    for instr in instr_link.list_instruments():
        instr_des = instr_link.get_instrument(*instr)
        instr_def = instr_des.definition

        if instr_def.kind == INSTR_KIND_PUT or instr_def.kind == INSTR_KIND_CALL:
            expr = instr_def.expiry_date
            expr_dt = date(expr.year, expr.month, expr.day)

            if expr_dt in expiries:
                expr_index = expiries.index(expr_dt)

                greeks = instr_des.greeks
                if greeks is not None:
                    theo = instr_des.greeks.theo
                    offset = offset_dict[expr_index]

                    if theo - offset*multiplier > 750:
                        params = {
                            "bid_offset_override": -1*offset,
                            "ask_offset_override": offset
                        }

                        feedcode = instr_def.instr_id.feedcode
                        instr_link.set_params("XHKF", feedcode, params)

    print("Submitting updates")
    instr_link.submit_updates()
    print("Finished submitting updates")

try:
    main()
except:
    traceback.print_exc()

